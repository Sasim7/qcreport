#!/bin/bash

n=$#

out=${@: -1}

sample_dirs=

for arg in "$@"
do
 step_dir=$(dirname $arg)
 base_dir=$(dirname $step_dir)
 sample_dirs+=($base_dir)
done

input_dirs=${sample_dirs[*]}

find $input_dirs -type f -name '*command.log' -exec cat {} + >> ${input_dirs}/commandlist.txt


