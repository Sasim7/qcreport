#! /usr/bin/python

"""BillionBases QC report 

This program is part of BillionBases quality control report generator for ngs pipelines
This program takes command line arguments so that it can run the appropriate module to create only needed module report
"""

__author__ = "Sasikanth Manne"
__version__ = "$Revision: 1.0 $"
__date__ = "$Date:2016/11/06 $"
__copyright__ = "Copyright (c) 2016 Sasikanth Manne"
__license__ = "Python"

import sys,os,argparse
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np

parser = argparse.ArgumentParser(description='Create QCreport plots for sequence,alignment and variant files')
parser.add_argument("-f",action='store_true',help="to summarize quality check of fastq files")
#parser.add_argument("-t",action='store_true',help="to summarize read trimming results")
parser.add_argument("-aln",action='store_true',help="to summarize alignment statistics")
parser.add_argument("-var",action='store_true',help="to summarize variant calling")
parser.add_argument("-report",action='store_true',help="to create a html report")
parser.add_Argument("-dir",action='store_true',help="directory with multiqc data")

args = parser.parse_args()

# #print args

def plot_fastqc(fname):
	fastqcdata = pd.read_table(fname,delimiter='\t',names=None)
	samples=fastqcdata['Sample']
	seq_len=fastqcdata['avg_sequence_length']
	Tot_dedup=fastqcdata['total_deduplicated_percentage']
	Tot_seqs=fastqcdata['Total Sequences']
	y_pos = np.arange(len(samples))
	plt.figure(figsize=(20,20))
	plt.bar(y_pos, seq_len, align='center', alpha=0.5)
	plt.xticks(y_pos, samples,rotation='vertical')
	plt.ylabel('Read length',fontsize=20)
	plt.tick_params(axis='both', which='major', labelsize=20)
	plt.savefig('Read_length.png',dpi=(600))
	plt.close()
	plt.figure(figsize=(20,20))
	plt.bar(y_pos, Tot_seqs, align='center', alpha=0.5)
	plt.xticks(y_pos, samples,rotation='vertical',fontsize=20)
	plt.ylabel('Total number of sequences',fontsize=20)
	plt.savefig('Number_of_sequences.png',	dpi=(600))
	plt.close()
	plt.figure(figsize=(20,20))
	plt.bar(y_pos, 100-Tot_dedup, align='center', alpha=0.5)
	plt.xticks(y_pos, samples,rotation='vertical',fontsize=20)
	plt.ylabel('Percent duplication',fontsize=20)
	plt.savefig('Percent_duplication.png',	dpi=(600))
	plt.close()
	
def plot_bam(bamstats):
	bamstatsdata=pd.read_table(bamstats,delimiter='\s+')
	params=['MAPQ>=30','MAPQ<30','MAPQ<20','MAPQ<10','Unmapped']
	plt.figure(figsize=(70,70))
	bamstatsdata.plot.pie(subplots=True,labels=params)
	plt.savefig("Bam_pie.png",dpi=(300))

def plot_bamsumm(bamsummary):
	bamsumdata=pd.read_table(bamsummary,delimiter="\s+")
	plt.figure(figsize=(50,50))
        df=pd.DataFrame(bamsumdata)
	df.plot.bar()
        plt.savefig("Bam_bar.png",dpi=(300))
	
def plot_var(varstats):
	varstatsdata=pd.read_table(varstats,delimiter='\s+')
	vardf=pd.DataFrame(varstatsdata)
	vardf.columns=['ST','id','type','count','misc']
	samples=vardf['type']
	count=vardf['count']
	y_pos = np.arange(len(samples))
	plt.figure(figsize=(20,20))
	plt.bar(y_pos,count,align='center', alpha=0.5)
	plt.xticks(y_pos,samples,rotation='vertical',fontsize=20)
	plt.ylabel('Counts',fontsize=20)
	plt.savefig('Substitutions.png',dpi=(300))
	plt.close()
	

def plot_indeldist(indelstats):
        indeldist=pd.read_table(indelstats,delimiter='\s+')
	indf=pd.DataFrame(indeldist)
	indf.columns=['ST','id','length','count','misc1','misc2','misc3']
	samples=indf['length']
	count=indf['count']
	y_pos = np.arange(len(samples))
	plt.figure(figsize=(30,20))
	plt.bar(y_pos,count,align='center', alpha=0.5)
	plt.xticks(y_pos,samples,rotation='vertical',fontsize=20)
	plt.ylabel('Counts',fontsize=20)
	plt.savefig('indel_stats.png',dpi=(300))
	plt.close()
	
try:
	if args.f:
		plot_fastqc(args.dir/multiqc_data/'multiqc_fastqc.txt')
	
	if args.aln:
		plot_bam('allSampleBamStats.txt')
		plot_bamsumm('allSamplesBamsummary.txt')

	if args.var:
		plot_var('allvcfsubstitutions.txt')
		plot_indeldist('allindeldists.txt')	
	
	if args.report:
		f = open('BB_report.html','w')
		message = """<html>
		<head>
		<style>
		div.container {
    	width: 100%;
    	border: 1px solid gray;
		}
		header, footer {
    	padding: 1em;
    	color: white;
    	background-color: black;
    	clear: left;
    	text-align: center;
		}
		nav {
    	float: left;
    	max-width: 160px;
    	margin: 0;
    	padding: 1em;
		}
		nav ul {
    	list-style-type: none;
    	padding: 0;
		}
		nav ul a {
    	text-decoration: none;
		}
		article {
    	margin-left: 170px;
    	border-left: 1px solid gray;
    	padding: 1em;
    	overflow: hidden;
		}
		</style>
		</head>
		<body>
		<div class="container">
		<header>
		<h1>BillionBases quality report</h1>
		<img src="BB_testlog2.png" align="middle" style="width:304px;height:228px;">
		</header>
		<nav>
		<ul>
   		 <li><a href="#">Fastqc report</a></li>
   		 <li><a href="#">Alignment report</a></li>
			</ul>
		</nav>
		<article>
		<h1 style="text-align:center;font-size:300%;">Quality report of the samples</h1>
		<p style="font-size:160%;">Read length of samples</p>
		<img src="Read_length.png" align="middle" style="width:600px;height:600px;">
		<p style="font-size:160%;">Number of sequences of samples</p>
		<img src="Number_of_sequences.png" align="middle" style="width:600px;height:600px;">
		<p style="font-size:160%;">Duplication of reads</p>
		<img src="Percent_duplication.png" align="middle" style="width:600px;height:600px;">
		<p style="font-size:160%;">Mapping quality</p>
		<img src="Bam_pie.png" align="middle" style="width:600px;height:600px;">
		</article>
		</div>
		</body>
		</html>"""

		f.write(message)
		f.close()

except:
	pass
