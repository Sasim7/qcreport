#wget https://download1.rstudio.org/rstudio-1.0.153-i686-fedora.tar.gz

#tar -zxvf rstudio-1.0.153-i686-fedora.tar.gz

#sudo cp rstudio-1.0.153/bin/pandoc/pandoc /usr/local/bin

#sudo cp rstudio-1.0.153/bin/pandoc/pandoc-citeproc /usr/local/bin


#Script to start Deseq2 run

design_file=$1

#echo $design_file

out_dir=$2

#echo $out_dir

sample_dirs=

for arg in "$@"
do
 #step_dir=$(dirname $arg)
 #base_dir=$(dirname $step_dir)
 sample_dirs+=($arg)
done

#unset 'sample_dirs[${#sample_dirs[@]}-1]'
#unset 'sample_dirs[${#sample_dirs[@]}-1]'

#n=$#

#shift 2

input_dirs=${sample_dirs[*]}

echo "$input_dirs"

#cp /usr/local/bin/Deseq2_report.Rmd $out_dir

#cp /usr/local/bin/header.html $out_dir

Rscript /usr/local/bin/Run_Deseq2.R $input_dirs


#Rscript -e 'rmarkdown::render("Kallisto_Deseq2_report.Rmd")' $input_dirs