#wget https://download1.rstudio.org/rstudio-1.0.153-i686-fedora.tar.gz

#tar -zxvf rstudio-1.0.153-i686-fedora.tar.gz

#sudo cp rstudio-1.0.153/bin/pandoc/pandoc /usr/local/bin

#sudo cp rstudio-1.0.153/bin/pandoc/pandoc-citeproc /usr/local/bin

out_dir=$1
input_dir=$2

sample_dirs=

for arg in "$@"
do
 #step_dir=$(dirname $arg)
 #base_dir=$(dirname $step_dir)
 sample_dirs+=($arg)
done

#unset 'sample_dirs[${#sample_dirs[@]}-1]'
#unset 'sample_dirs[${#sample_dirs[@]}-1]'

#n=$#

#shift 2

input_dirs=${sample_dirs[*]}

echo "$input_dirs"

cp /usr/local/bin/BBreport.Rmd $out_dir

cp /usr/local/bin/header.html $out_dir

Rscript /usr/local/bin/Run_report.R $input_dirs


#Rscript -e 'rmarkdown::render("Kallisto_Deseq2_report.Rmd")' $input_dirs