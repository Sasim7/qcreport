#!/usr/bin/bash

#Bash Script to run multiqc at the sample level of the pipeline and create summary statistics for fastq,bam & vcf files
#Finds the necessary files using keywords and extracts statistics and creates tab delimited files which will be used for QCreport_plot.py
#to create plots

#Previously created positional arguments for the script so as to run only parts of the code,removed it ,now runs everything if it finds a file


#if [ $# -ne 2 ]; then
#    echo $0: usage: qc_report_test.sh yes no, first argument as yes to get bam stats and second argument as yes to get vcf stats
#    exit 1
#fi

# Run default multiqc on the current folder to create multiqc_data folder which will be used by QCreport_plot.py

sample_dirs=

for arg in "$@"
do
 step_dir=$(dirname $arg)
 base_dir=$(dirname $step_dir)
 proj_dir=$(dirname $base_dir)
 sample_dirs+=($base_dir)
done

input_dirs=${sample_dirs[*]}

multiqc -d $input_dirs -o $proj_dir/qc_report

for arg in "$@"
do
 $bam=$arg

b=`echo $bam | awk -F "/" '{print $NF}'`

echo $b

samtools sort $bam ${bam}_sort
samtools index ${bam}_sort.bam

samtools idxstats ${bam}_sort.bam  | awk '{a+=$3; b+=$4} END {print a+b"\n"a"\n"b}' > ${b}_bamsummary.txt

q10=$(samtools view -q10 ${bam}_sort.bam  | wc -l)

q20=$(samtools view -q20 ${bam}_sort.bam  | wc -l)

q30=$(samtools view -q30 ${bam}_sort.bam  | wc -l)

q3=$(samtools view -q3 ${bam}_sort.bam  | wc -l)

q1=$(samtools view -f4 -c ${bam}_sort.bam)

echo $q3,$q10,$q20,$q30

p1=`expr $q20 - $q30`
p2=`expr $q10 - $q20`
p3=`expr $q3 - $q10`

echo $b	> ${bam}_bamstats.txt
echo $q30 >> ${bam}_bamstats.txt
echo $p1 >> ${bam}_bamstats.txt
echo $p2 >> ${bam}_bamstats.txt
echo $p3 >> ${bam}_bamstats.txt
echo $q1 >> ${bam}_bamstats.txt


echo "Total_reads"> 1_bamsummary.txt

echo "Mapped_reads">> 1_bamsummary.txt

echo "Unmapped_reads">> 1_bamsummary.txt

echo "SampleName" > 1_bamstats.txt
echo "MAPQ>=30">> 1_bamstats.txt
echo "MAPQ<30">> 1_bamstats.txt
echo "MAPQ<20">> 1_bamstats.txt
echo "MAPQ<10">> 1_bamstats.txt
echo "Unmapped">> 1_bamstats.txt

done


find $proj_dir -name '*_bamstats.txt' | sort | xargs paste | column -s $'\t' -t > allSampleBamStats.txt

find $proj_dir -name '*_bamsummary.txt' | sort | xargs paste | column -s $'\n' -t > allSamplesBamsummary.txt
