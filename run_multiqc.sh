#!/bin/bash

n=$#

out=${@: -1}

sample_dirs=

for arg in "$@"
do
 step_dir=$(dirname $arg)
 base_dir=$(dirname $step_dir)
 sample_dirs+=($base_dir)
done

unset 'sample_dirs[${#sample_dirs[@]}-1]'

input_dirs=${sample_dirs[*]}

/home/ec2-user/tools/MultiQC/1.3/scripts/multiqc -d $input_dirs -o $out
